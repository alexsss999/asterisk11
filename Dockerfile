FROM centos:6.9

MAINTAINER alex-saf alex-saf@mail.ru

# Обновляем систему+Добавляем необходимые репозитарии и устанавливаем пакеты
RUN \
    yum -y clean all && \
    yum -y install epel-release && \
    yum erase abrt abrt-lib NetworkManager NetworkManager-glib bluez-libs spice-vdagent && \
    yum -y install "kernel-devel-uname-r == $(uname -r)" "kernel-firmware-uname-r == $(uname -r)" && \
    yum -y install curl curl-devel newt-devel mlocate lynx tar wget nmap bzip2 mod_ssl crontabs vixie-cron \
                gcc gcc-c++ bison bison-devel openssl openssl-devel \
                perl perl-Net-SSLeay perl-Crypt-SSLeay libtermcap-devel ncurses-devel doxygen \
                speex speex-devel unixODBC unixODBC-devel libtool-ltdl libtool-ltdl-devel \
                mysql-connector-odbc mysql mysql-devel mysql-server supervisor \
                flex screen netpbm-progs \
                spandsp spandsp-devel libxml2-devel svn libtiff libtiff-devel ImageMagick \
                ghostscript ghostscript-fonts sqlite-devel libuuid libuuid-devel libsrtp libsrtp-devel \
                iksemel iksemel-devel gmime gmime-devel pjproject pjproject-devel jansson jansson-devel \
                gsm-devel libogg-devel libvorbis-devel libss7 libss7-devel libpri libpri-devel \
                rsyslog cronie-noanacron
                
#RUN \
#    /bin/cp -a /etc/pam.d/crond /etc/pam.d/crond.org && \
#    sed -i -e 's/^\(session\s\+required\s\+pam_loginuid\.so\)/#\1/' /etc/pam.d/crond && \
#    mkdir -p /etc/supervisord.d && mkdir --mode 777 -p /var/log/supervisor && chmod -R 777 /var/run && \
#    /bin/mv -f /etc/supervisord.conf /etc/supervisord.conf.org && \
#    echo 'NETWORKING=yes' >> /etc/sysconfig/network && \
#    ln -sf /usr/share/zoneinfo/UTC /etc/localtime

#RUN \
#    curl -sf -o /tmp/dahdi-complete.tar.gz -L http://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/dahdi-linux-complete-2.10.2+2.10.2.tar.gz && \
#    mkdir -p /tmp/dahdi-complete && \
#    tar -xzf /tmp/dahdi-complete.tar.gz -C /tmp/dahdi-complete --strip-components=1 && \
#    mkdir /lib/modules/`uname -r` && \
#    ln -sf /usr/src/kernels/`uname -r` /lib/modules/`uname -r`/build && ln -sf /lib/modules/`uname -r`/build /lib/modules/`uname -r`/source
#&& \
#WORKDIR    /tmp/dahdi-complete

#RUN \
#    make all && make install && ldconfig

RUN \
    curl -sf -o /tmp/asterisk11.tar.gz -L http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-11-current.tar.gz && \
    mkdir -p /tmp/asterisk11 && \
    tar -xzf /tmp/asterisk11.tar.gz -C /tmp/asterisk11 --strip-components=1 
#&& \
WORKDIR    /tmp/asterisk11/contrib/scripts
#&& \
RUN    ./get_mp3_source.sh && \
    echo '\n' | ./get_ilbc_source.sh && \
    /bin/cp -rf addons /tmp/asterisk11/ && /bin/cp -rf codecs /tmp/asterisk11/
WORKDIR    /tmp/asterisk11
RUN    ./configure CFLAGS='-g -O2' --libdir=/usr/lib64 && \
    make menuselect.makeopts && \
    menuselect/menuselect \
        --disable BUILD_NATIVE \
	--enable format_mp3 \
	--enable app_mysql \
	--enable cdr_mysql \
	--enable chan_dahdi \
	--enable codec_ilbc \
	--enable res_http_websocket \
	--enable res_config_mysql \
	--enable res_rtp_asterisk \
	--enable res_srtp \
	--enable astman \
	--enable aelparse \
    menuselect.makeopts && \
    make && make install && make config && make install-logrotate && make samples && \
    sed -i 's/rtpend=20000/rtpend=11000/g' /etc/asterisk/rtp.conf && \
    yum -y clean all && /bin/rm -rf /tmp/*
            
COPY supervisord.conf /etc/
#COPY asterisk.conf /etc/supervisord.d/

# Объявляем, какие директории мы будем подключать
#VOLUME /etc/asterisk /var/lib/asterisk /var/spool/asterisk

# Объявляем, какие порты этот контейнер будет транслировать
EXPOSE 4569/udp 5060/udp 8088 
#10000-11000/udp

#WORKDIR /etc

#ENTRYPOINT ["/bin/bash", "-e", "/init/entrypoint"]

CMD ["/usr/bin/supervisord", "--configuration=/etc/supervisord.conf"]
